package com.example.lesson3;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v4.app.NavUtils;
import android.text.InputType;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

public class Activity2 extends Activity {

	EditText edittext, edittext2;
	public static final int EXTRA_REQUEST = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity2);
		// Show the Up button in the action bar.
		setupActionBar();		
		edittext = (EditText) findViewById(R.id.edittext);
		edittext.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
		edittext2 = (EditText) findViewById(R.id.edittext2);
		edittext2.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void move(View view)
	{
		Intent intent = new Intent();
		String ans1, ans2;
		ans1 = edittext.getText().toString();
		ans2 = edittext2.getText().toString();
		intent.putExtra("ans1",ans1);
		intent.putExtra("ans2",ans2);
		setResult(RESULT_OK, intent);
        finish();


	}

}
