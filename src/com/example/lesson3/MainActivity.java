package com.example.lesson3;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	TextView textview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview = (TextView) findViewById(R.id.textview);		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
        case Activity2.EXTRA_REQUEST:
			if (resultCode == RESULT_OK)
			{
				String text1 = (String) data.getStringExtra("ans1");
				double num1 = Integer.parseInt(text1);
				String text2 =  (String) data.getStringExtra("ans2");
				double num2 = Integer.parseInt(text2);
				double sum =  num1+num2;
				textview.setText(String.valueOf(sum));
			}
			break;
       }
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void startactivity2(View view)
	{
		Intent intent = new Intent(MainActivity.this, Activity2.class);
		startActivityForResult(intent, Activity2.EXTRA_REQUEST);

	}

}
